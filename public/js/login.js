/**
 * @author Elvis
 */

$(function() {
	$("#reset").submit(function(event) {
	  var form = $(this);
	  $.ajax({
		url : form.attr('action'),
		type : 'POST',
		data : form.serialize(),
		beforeSend: function( xhr ) {
			$("[name='reset']").val('Enviando ........');
		},
		error : function() {
		  alert('ocurrio un error');
		},
		success : function(data) {
		  $("blockquote").before(data);
		  $("#e-mail").val('');
		  $("[name='reset']").val('Reset');
		}
	  });
	  return false;
	});
});

