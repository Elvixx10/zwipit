<?php
    header("Content-type: text/css");
    $iconBarBackground = "#EEEEEE";
    $heroHeadingBackgroundColor = "#FFFFFF";
    $heroHeadingHColor = "#E73F97";
    $spanBorderColor = "#FF5784";
    $spanColor = "#FFFFFF";
    $spanBackground = "#FF0084";
 
    /*TODO: checar variables dinamicas
     * icon-bar a:hover{
        background-color: #FFFFFF !important;
    }
    
    .icon-bar a:hover > label{
        color:#000000 !important;
    }*/
    
    $ccs="
    .box {
        -webkit-box-shadow: 0 0px 4px #777, 0 0 1px #CCC inset;
        -moz-box-shadow: 0 0px 4px #777, 0 0 1px #CCC inset;
        box-shadow: 0 0px 4px #777, 0 0 1px #CCC inset;
    }
    
    .icon-bar {
        background-color: {$iconBarBackground} !important;
    }
    
    .icon-bar a > label{
        color:#000000 !important;
    }
    
    .icon-bar a:hover{
		background-color: #FFFFFF !important;
	}
	
	.icon-bar a:hover > label{
		color:#000000 !important;
	}
          
    .hero-heading {
      margin-top: -1.875rem;
      background-color: {$heroHeadingBackgroundColor};
      padding: 2.5rem 0 2.5rem 0;
      margin-bottom: 3.125rem;
      border-bottom: 1px solid #EDEBEB;
    }
    
    .hero-heading h3, .hero-heading h4 {
      color: {$heroHeadingHColor};
      font-style:italic;
    }
    
    .hero-heading h1 {
      font-size: 3.35rem;
      line-height: 1;
    }
    
    .hero-heading h1  img{
        max-width: 300px;
        max-height: 150px;
    }

    .legend {
        font-size: 16px;
        font-weight: bold;
        color: {$spanBackground};
    }

    .span {
        border-color: {$spanBorderColor} !important;
        color: {$spanColor} !important;
        background-color: {$spanBackground} !important;
    }
    
    @media (max-width: 300px) {
      .hero-heading h1  img{
        max-width: 200px;
        max-height: 150px;
      }
    }
    ";
    echo $ccs;
?>