<?php
namespace Login\Form;
use Zend\Form\Element;
use Zend\Form\Form;

class FormPassword extends Form 
{
    public $token;

    public function __construct($name = null)
    {
        $this->token = "__".md5(uniqid(mt_rand(), true))."__";

        parent::__construct($name);
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'e-mail', 
            'type' => 'Zend\Form\Element\Email', 
            'attributes' => array(
                'placeholder' => 'correo@example.com', 
                'required' => 'required', 
                'maxlength' => 30,
                'id' => 'e-mail'
            ), 
            'options' => array(
                'label' => 'Correo Electrónico', 
            ), 
        )); 

        $this->add(array(
            'name' => 'token', 
            'type' => 'Zend\Form\Element\Hidden', 
            'attributes' => array( 
                'value' => $this->token, 
            ), 
            'options' => array(
            ), 
        ));
        
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Reset',
                'class' => 'button span',
            ),
        ));
    } 
} 