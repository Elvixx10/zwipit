<?php
namespace Login\Form;
use Zend\Form\Element;
use Zend\Form\Form;

class FormLogin extends Form 
{
    public $token;

    public function __construct($name = null)
    {
        $container = new \Zend\Session\Container('item');
        $this->token = $container->token = "__".md5(uniqid(mt_rand(), true))."__";

        parent::__construct($name);
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'usuario', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array(
                'placeholder' => 'Usuario', 
                'required' => 'required', 
                'pattern' => '[a-zñA-ZÑ0-9]+',
                'maxlength' => 25
            ), 
            'options' => array(
                // 'label' => 'Usuario', 
            ), 
        )); 
 
        $this->add(array(
            'name' => 'password', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array(
                'placeholder' => '**********', 
                'required' => 'required', 
                'maxlength' => 20
            ), 
            'options' => array(
                //'label' => 'Contraseña', 
            ),
        )); 
 
        $this->add(array(
            'name' => 'token', 
            'type' => 'Zend\Form\Element\Hidden', 
            'attributes' => array( 
                'value' => $this->token, 
            ), 
            'options' => array(
                //'label' => '',
            ), 
        ));
        
        
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Login',
                'class' => 'button span',
            ),
        ));
    } 
} 