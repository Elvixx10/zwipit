<?php
namespace Login\Model\Validator; 

use Zend\InputFilter\Factory as InputFactory; 
use Zend\InputFilter\InputFilter; 
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 
use Zend\Validator\Regex;

class PasswordValidator implements InputFilterAwareInterface 
{
    public $email;
    public $token;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->email  = (isset($data['e-mail'])) ? $data['e-mail'] : null;
        $this->token   = (isset($data['token'])) ? $data['token'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter) 
    { 
        throw new \Exception("Not used");
    } 
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) 
        {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            
        
        $inputFilter->add($factory->createInput([
            'name' => 'e-mail', 
            'required' => true, 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim')
            ), 
            'validators' => array(
               array(
                 'name' => 'string_length',
                 'options' => array(
                   'max' => 30
                 ),
               ),
               array(
                 'name' => 'Regex',
                 'options' => array(
                   'pattern' => '/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',  
                   'messages' => array(
                      Regex::NOT_MATCH => 'Debe de especificar un correo electrónico valido.'
                   ),
                 ),
               )
            )
        ]));

            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    } 
}
