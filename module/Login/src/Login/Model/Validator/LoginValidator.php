<?php
namespace Login\Model\Validator; 

use Zend\InputFilter\Factory as InputFactory; 
use Zend\InputFilter\InputFilter; 
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface; 
use Zend\Validator\Regex;

class LoginValidator implements InputFilterAwareInterface 
{
    public $usuario;
    public $password;
    public $token;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->usuario   = (isset($data['usuario'])) ? ucfirst($data['usuario']) : null;
        $this->password  = (isset($data['password'])) ? $data['password'] : null;
        $this->token   = (isset($data['token'])) ? $data['token'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter) 
    { 
        throw new \Exception("Not used");
    } 
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) 
        {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            
        
        $inputFilter->add($factory->createInput([
            'name' => 'usuario', 
            'required' => true, 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim')
            ), 
            'validators' => array(
               array(
                 'name' => 'string_length',
                 'options' => array(
                   //'encoding' => 'UTF-8',
                   'max' => 25
                 ),
               ),
               array(
                 'name' => 'Regex',
                 'options' => array(
                   'pattern' => '/^[a-zñA-ZÑ0-9]+$/',  
                   'messages' => array(
                      Regex::NOT_MATCH => 'El campo solo permite letras y números.'
                   ),
                 ),
               )
            )
        ]));
 
        $inputFilter->add($factory->createInput([
            'name' => 'password', 
            'required' => true, 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim')
            ), 
            'validators' => array(
               array(
                 'name' => 'string_length',
                 'options' => array(
                   //'encoding' => 'UTF-8',
                   'max' => 20
                 ),
               ), 
            ), 
        ]));
 
            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    } 
} 