<?php
namespace Login\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface; 

class relDataUser implements ServiceLocatorAwareInterface
{
    protected $table = 'relDataUser';
    protected $idDataUser;
    protected $name;
    protected $paternal;
    protected $maternal;
    protected $email;
    protected $date;
    protected $idUser;
    protected $idRole;
    protected $idBranchOffice;
    protected $serviceLocator;

	public $catUser;
	
	public function __construct()
	{
		$this->catUser = (new \Application\Model\catUser())->getTable();
	}
    
    public function getDataEmail($email)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT email, name, paternal, maternal, idUser FROM {$this->getTable()} WHERE email LIKE '%{$email}'", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
            foreach ($for as $value) 
            {
                $this->setEmail($value['email']);
                $this->setName($value['name']);
                $this->setPaternal($value['paternal']);
                $this->setMaternal($value['maternal']);
                $this->setIdUser($value['idUser']);
            }
            $msg= TRUE;
        }
        return $msg;
    }
	
	public function getBranchOffice($idUser)
	{
		$result = $this->getServiceLocator()->get('Adapter')->query("SELECT a.idBranchOffice FROM {$this->getTable()} AS a 
		LEFT JOIN {$this->catUser} AS b ON b.idUser = a.idUser AND b.status = 1
		WHERE a.idUser = 1", Adapter::QUERY_MODE_EXECUTE);
		$for = $result->toArray();
		// $for = array();
		$msg = FALSE;
        if(!empty($for))
		{
			foreach ($for as $value) 
			{
				$this->setIdBranchOffice($value['idBranchOffice']);	
			}
			$msg= TRUE;
		}
		return $msg;
	}

    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function setIdDataUser($idDataUser)
    {
        $this->idDataUser=$idDataUser;
    }
    
    public function getIdDataUser()
    {
        return $this->idDataUser;
    }
    
    public function setName($name)
    {
        $this->name=$name;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setPaternal($paternal)
    {
        $this->paternal=$paternal;
    }
    
    public function getPaternal()
    {
        return $this->paternal;
    }
    
    public function setMaternal($maternal)
    {
        $this->maternal=$maternal;
    }
    
    public function getMaternal()
    {
        return $this->maternal;
    }
    
    public function setEmail($email)
    {
        $this->email=$email;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setDate($date)
    {
        $this->date=$date;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function setIdUser($idUser)
    {
        $this->idUser=$idUser;
    }
    
    public function getIdUser()
    {
        return $this->idUser;
    }
    
    public function setIdRole($idRole)
    {
        $this->idRole=$idRole;
    }

    public function getIdRole()
    {
        return $this->idRole;
    }
    
    public function setIdBranchOffice($idBranchOffice)
    {
        $this->idBranchOffice=$idBranchOffice;
    }
    
    public function getIdBranchOffice()
    {
        return $this->idBranchOffice;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
