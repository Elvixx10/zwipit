<?php
namespace Login\Factory\Storage;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

class AuthenticationServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator){
        $dbAdapter = $serviceLocator->get('Adapter');
        $dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter, 'catUser', 'username', 'password', 'MD5(?)');
        $dbTableAuthAdapter -> getDbSelect() -> where('status = 1');
        $authService = new AuthenticationService($serviceLocator->get('AuthStorage'), $dbTableAuthAdapter);
        return $authService;
    }
}