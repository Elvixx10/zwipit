<?php
namespace Login\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Login\Controller\IndexController;

class AuthControllerServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator){
    	
        $identityManager = $serviceLocator -> getServiceLocator() -> get('IdentityManager');        
		$controller = new IndexController($identityManager);		
        return $controller;
    }
}