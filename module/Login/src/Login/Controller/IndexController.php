<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Login\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Login\Form\FormLogin;
use Login\Model\Validator\LoginValidator;
use Login\Form\FormPassword;
use Login\Storage\IdentityManagerInterface;

class IndexController extends AbstractActionController
{
    protected $identityManager;
    
    public function __construct(IdentityManagerInterface $identityManager)
    {
        $this->identityManager = $identityManager;
    }
     
    public function indexAction()
    {
        $view = new  ViewModel();
        $login = new FormLogin('login');
        $reset = new FormPassword('reset');
        
        if($this->identityManager->hasIdentity())
        {
           return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
        }
        
        $this->authenticate($login, $view);
        $view->setVariable('login', $login)
             ->setVariable('reset', $reset);
        return $view;
    }

    protected function authenticate($form, $view)
    {
        $request = $this->getRequest(); 
        if ($request->isPost()) 
        {
            $validator = new LoginValidator();
            {
              $form->setInputFilter($validator->getInputFilter());
              $form->setData($request->getPost()->toArray());
            }
            
            if($form->isValid())
            {
               $container = new \Zend\Session\Container('item'); 
               //$container->getManager()->destroy();
               if($request->getPost('valid'))
               {
                  $validator->exchangeArray($form->getData());
                  $result = $this->identityManager->login($validator->usuario, $validator->password);
                  if($result->isValid()) 
                  {
                     $identityRow = $this->identityManager->getAuthService()->getAdapter()->getResultRowObject();
                     $this->identityManager->storeIdentity(
                        array(
                          'idUser' => $identityRow->idUser,
                          'user' => $identityRow->username,
                          'date' => date('Y-m-d H:m:i'),
                          'ipAddress'  => $this -> getRequest() -> getServer('REMOTE_ADDR'),
                          'userAgent'  => $request -> getServer('HTTP_USER_AGENT'),                                   
                        )
                     );
                     
                     return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
                  }
                  else 
                  {
                     $view->setVariable('msg', 'Usuario  o contrase&ntilde;a no válido.')
                          ->setVariable('class', 'alert');
                  }
               }
               else
               {
                 $view->setVariable('msg', 'El token de acceso ha expirado por favor refresca la p&aacute;gina.')
                      ->setVariable('class', 'alert');
               }
               
            }
            //else{print_r($login->getMessages());}
        }
   }

   public function logoutAction()
   {
       //$locator= $this->getServiceLocator();
       //$json = $locator->get('&Session')->session();
       // $update = $locator->get('update');
       // $update->setTable('catAdmUsuarios');
       // $update->setUpdate(array('statusLinea' => 0), array('idRvt' => $json['idRvt']), $this -> params() -> fromRoute('controller'), $this -> params() -> fromRoute('action'));
       $this->identityManager->logout();
       return $this->redirect()->toRoute('home', array('controller' => 'Index', 'action' => 'index'));
    }
}
