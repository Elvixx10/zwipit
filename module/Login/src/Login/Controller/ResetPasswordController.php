<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Login\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Login\Form\FormPassword;
use Login\Model\Validator\PasswordValidator;

class ResetPasswordController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();
        $form = new FormPassword();
        $request = $this->getRequest();
        $locator = $this->getServiceLocator();
        
        if($request->isPost() && $request->isXmlHttpRequest())
        {
            $validator = new PasswordValidator();
            {
              $form->setInputFilter($validator->getInputFilter());
              $form->setData($request->getPost()->toArray());
            }
            
            if($form->isValid())
            {
                $validator->exchangeArray($form->getData()); 
                $result = $locator->get('RelDataUser');
                
                $form->get('e-mail')->setMessages(array("No se ha encontrado ningún cliente con esa dirección de correo."));
                $view->setVariable('msg', $form->getMessages())
                     ->setVariable('class', 'alert');

                if($result->getDataEmail($validator->email))
                {
                    $url = $request->getUri()->getScheme() . '://' . $request->getUri()->getHost();
                    $viewMail = new ViewModel(array(
                      'title' => 'Proceso de recuperación de contraseña',
                      'email'=> $result->getEmail(),
                      'name' => $result->getName(),
                      'paternal' => $result->getPaternal(),
                      'maternal' => $result->getMaternal(),
                      'url' => $url."/profile/password/".$validator->token."/".$result->getIdUser()
                    ));
                    
                    $viewMail->setTemplate('application/emails/render-mail');
                    $config = $locator->get('ViewRenderer');
                    $html = $config->render($viewMail);

                    $mail = $this->Mail();
                    $parameters = [
                      'to' => [
                        [
                          'correo' =>$result->getEmail(),
                          'cc' => false
                        ]
                      ],
                      'asunto' => 'Proceso de recuperación de contraseña',
                      'body' => $html,
                      'from' => 'errores@sia.dev'
                    ];
                    $mail-> setMailer($parameters, 'text/html');
                    if((new \Zend\Http\Response())->getStatusCode() == 200)
                    {
                        $insert = $locator->get('insert');
                        $insert->setTable('histRecovery');
                        $insert->setInsert(
                          array('token' => $validator->token,'idUser' => $result->getIdUser()), 
                          $this -> params() -> fromRoute('controller'), 
                          $this -> params() -> fromRoute('action')
                        );
                        $form->get('e-mail')->setMessages(array("Se ha enviado un correo electrónico a {$result->getEmail()} para el proceso de recuperacion de contraseña."));
                        $view->setVariable('msg', $form->getMessages())
                             ->setVariable('class', 'success');
                    }
                }
                
         }
         else
         {
           $view->setVariable('msg', $form->getMessages())
                ->setVariable('class', 'alert');
         }
            
        }
        $view->setTerminal($request->isXmlHttpRequest());
        return $view;
    }
}