<?php
namespace Login\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mail;
use Zend\Mail\Transport\Smtp;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Mime;
use Zend\Mail\Message;


class Mailer extends AbstractPlugin
{
    /**
     * @param array $parameters arrreglo de parametros tales como
     * datod adjuntos, las personas a las que se les enviara,
     * el asunto del correo.
     *  Tomese el siguiene ejemplo de un envio de correo
     * array(
     *    'to' => array(
     *        array(
     *            'correo' =>'micorreo@dominio.com'
     *            'cc' => false
     *        array(
     *            'correo' =>'micorreo@dominio.com'
     *            'cc' => true
     *        ),
     *        array(
     *            'correo' =>'micorreo@dominio.com'
     *        ),
     *    ),
     *    'asunto' => 'prueba de correo',
     *    'attachments' => array(
     *        'rutaDelArchivol.txt',
     *        'OtrarutaDelArchivol.jpg',
     *        'OtrarutaDelArchivol.png'
     *    ),
     *    'body' => 'Prueba del cuerpo del correo',
     *    'from' => 'deMi@miDominio.com'
     * )
     */
     

    public function setMailer($parameters, $type)
    {
        $mail = new Message();

        $arrayAddresses = array_key_exists('to', $parameters) && !empty($parameters['to']) ? $parameters['to'] : array( array('correo' => 'miDefaultCorreo@mio.com'));
        $asunto = $parameters['asunto'] ? $parameters['asunto'] : '';
        $mail->setSubject($asunto);
        if (is_array($arrayAddresses)) 
        {
            foreach ($arrayAddresses as $destinatario) 
            {
                if (isset($destinatario['cc'])) 
                {
                    if ($destinatario['cc']) 
                    {
                        $mail->addCc($destinatario['correo']);
                    } 
                    else 
                    {
                        $mail->addTo($destinatario['correo']);
                    }
                } 
                else 
                {
                    $mail->addTo($destinatario['correo']);
                }
            }
        }

        $attachments = isset($parameters['attachments']) ? $parameters['attachments'] : array();

        $attachmentsEmail = array();
        $info = new \finfo();
        $this->setMaximumTime(count($attachments));
        foreach ($attachments as $attachementKey => $attachmentValue) 
        {
            if (file_exists($attachmentValue)) 
            {
                $mimePartAttach = new MimePart(fopen($attachmentValue, 'r'));
                $mimeType = $info->file($attachmentValue, FILEINFO_MIME_TYPE);
                $mimePartAttach->type = $mimeType;
                $mimePartAttach->encoding = Mime::ENCODING_BASE64;
                $mimePartAttach->filename = basename($attachmentValue);
                $mimePartAttach->disposition = Mime::DISPOSITION_ATTACHMENT;
                $attachmentsEmail[] = $mimePartAttach;
            }
        }

        $mimePart = new MimePart($parameters['body']);
        $mimePart->type = $type;
        $mailBody = new MimeMessage();
        $attachmentsEmail[] = $mimePart;
        $mailBody->setParts($attachmentsEmail);

        $mail->setBody($mailBody);

        $transport = new Smtp();
        $transport->setOptions($this->setSmtpOptions());
        $configSmtp = $transport->getOptions();
        $optionsConnection = $configSmtp->getConnectionConfig();
        $from = array_key_exists('from', $parameters) ? $parameters['from'] : $optionsConnection['username'];

        $mail->addFrom($from);
        $result = $transport->send($mail);
    }

    public function setSmtpOptions()
    {
        $transportOptions = new Mail\Transport\SmtpOptions(array());
        $transportOptions->setName('mail.gpmass.com');
        $transportOptions->setPort(587);
        $transportOptions->setHost('mail.gpmass.com');
        $transportOptions->setConnectionConfig(array('username' => 'seguros@tiprotec.com', 'password' => 'Nb9L8.2'));
        $transportOptions->setConnectionClass('login');
        return $transportOptions;
    }

    public function setMaximumTime($numberAttache)
    {
        $numberAttache = $numberAttache == 0 ? 1 : $numberAttache;
        set_time_limit($numberAttache * 50);
    }

}
