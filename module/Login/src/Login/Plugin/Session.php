<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright  2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */ 
namespace Login\Plugin;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Session extends AbstractPlugin implements ServiceLocatorAwareInterface 
{
    
    protected $serviceLocator;
        
    public function session()
    {
        return \Zend\Json\Json::decode($this->getServiceLocator() 
              -> get('AuthService')->getStorage() 
              -> getSessionManager()->getSaveHandler() 
              -> read($this->getServiceLocator()->get('AuthService')->getStorage()->getSessionId())
             , true);
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }  
}
    
?>