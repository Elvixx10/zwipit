<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Login;

use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManager;
use Zend\Json\Json;

class Module implements AutoloaderProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        if($e->getRequest()->isPost())
        {
           $container = new \Zend\Session\Container('item');
           $post = $e->getRequest();
           $token = ($container->token === $post->getPost('token')) ? 1 : 0 ;
           $post->getPost()->set('valid', $token);
        }
        
        $eventManager -> attach('route', array($this, 'checkAuthenticated'));
		// $eventManager -> attach('route', array($this, 'extendSession'));
    }
    
    public function isOpenRequest(MvcEvent $e)
    {
        if ($e->getRouteMatch()->getParam('controller') == 'Login\Controller\Index')
        {
            return true;
        }
        return false;
    }
    
    public function checkAuthenticated(MvcEvent $e)
    {
        if (!$this -> isOpenRequest($e)) 
        {
            $sm = $e -> getApplication() -> getServiceManager();
            if (!$sm -> get('AuthService') -> getStorage() -> getSessionManager()
                     -> getSaveHandler() -> read($sm -> get('AuthService') -> getStorage() -> getSessionId()))
            {
                if($e -> getRouteMatch() -> getParam('controller') == 'Login\Controller\Resetpassword')
                {
                   $e -> getRouteMatch() -> setParam('controller', 'Login\Controller\Resetpassword') -> setParam('action', 'index');
                }
                else if($e -> getRouteMatch() -> getParam('controller') == 'Profile\Controller\Password')
                {
                   $e -> getRouteMatch() -> setParam('controller', 'Profile\Controller\Password') -> setParam('action', 'index'); 
                }
                else if($e -> getRouteMatch() -> getParam('controller') == 'Profile\Controller\SendPassword')
                {
                   $e -> getRouteMatch() -> setParam('controller', 'Profile\Controller\SendPassword') -> setParam('action', 'index');
                }
                else
                {
                   $e -> getRouteMatch() -> setParam('controller', 'Login\Controller\Index') -> setParam('action', 'index');
                }
            }
        }
    }

 	// public function extendSession(MvcEvent $e) 
 	// {
        // $sm = $e->getApplication()->getServiceManager();
        // $user = $sm->get('AuthService')->getStorage()->getSessionManager()->getSaveHandler()->read($sm->get('AuthService')->getStorage()->getSessionId());
        // $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        // $viewModel->data = Json::decode($user, TRUE);
    // }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function init(ModuleManager $mm)
    {
        $mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__,
        'dispatch', function($e) {
            $sm = $e -> getApplication() -> getServiceManager();
            $user = $sm -> get('AuthService') -> getStorage() -> getSessionManager()
                 -> getSaveHandler() -> read($sm -> get('AuthService') -> getStorage() -> getSessionId());
            if($user){
               $e->getTarget()->layout('layout/login');
            }
        });
    }
}
