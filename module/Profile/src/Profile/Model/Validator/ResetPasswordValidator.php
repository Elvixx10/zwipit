<?php
namespace Profile\Model\Validator;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Regex;

class ResetPasswordValidator implements InputFilterAwareInterface 
{
    public $newPassword;
    public $ccPassword;
    public $token;
    public $idUser;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->newPassword  = (isset($data['newPassword'])) ? $data['newPassword'] : null;
        $this->ccPassword   = (isset($data['ccPassword'])) ? $data['ccPassword'] : null;
        $this->token = (isset($data['token'])) ? $data['token'] : null;
        $this->idUser = (isset($data['idUser'])) ? $data['idUser'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter) 
    { 
        throw new \Exception("Not used");
    } 
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) 
        {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            
        
        $inputFilter->add($factory->createInput([
            'name' => 'newPassword', 
            'required' => true, 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim')
            ), 
            'validators' => array(
               array(
                 'name' => 'string_length',
                 'options' => array(
                   'min' => 10,
                   'max' => 20
                 ),
               ),
               array(
                 'name' => 'Regex',
                 'options' => array(
                   'pattern' => '/^(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/',  
                   'messages' => array(
                      Regex::NOT_MATCH => 'La contraseña debe tener (mayúsculas, minúsculas, Número / Char Especial y minimo 10 Caracteres.'
                   ),
                 ),
               )
            )
        ]));
        
        $inputFilter->add($factory->createInput([
            'name' => 'ccPassword', 
            'required' => true, 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim')
            ), 
            'validators' => array(
               array(
                 'name' => 'string_length',
                 'options' => array(
                   'min' => 10,
                   'max' => 20
                 ),
               ),
               array(
                 'name' => 'Regex',
                 'options' => array(
                   'pattern' => '/^(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/',   
                   'messages' => array(
                      Regex::NOT_MATCH => 'La contraseña debe tener (mayúsculas, minúsculas, Número / Char Especial y minimo 10 Caracteres.'
                   ),
                 ),
               ),
               array(
                 'name' => 'Identical',
                   'options' => array(
                     'token' => 'newPassword'
                 ),
               ),
            )
        ]));

            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    } 
}
