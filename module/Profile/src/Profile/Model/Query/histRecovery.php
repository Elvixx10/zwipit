<?php
namespace Profile\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class histRecovery implements ServiceLocatorAwareInterface
{
    protected $table = 'histRecovery';
    protected $idRecovery;
    protected $token;
    protected $date;
    protected $status = 1;
    protected $idUser;
    
    public function getDataToken($token)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT idUser, token FROM {$this->getTable()} WHERE token = '{$token}' AND status = {$this->getStatus()}", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
            foreach ($for as $value) 
            {
                $this->setToken($value['token']);
                $this->setIdUser($value['idUser']);
            }
            $msg= TRUE;
        }
        return $msg;
    }

    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function setIdRecovery($idRecovery)
    {
        $this->idRecovery=$idRecovery;
    }
    
    public function getIdRecovery()
    {
        return $this->idRecovery;
    }
    
    public function setToken($token)
    {
        $this->token=$token;
    }
    
    public function getToken()
    {
        return $this->token;
    }
    
    public function setDate($date)
    {
        $this->date=$date;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function setStatus($status)
    {
        $this->status=$status;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
     public function setIdUser($idUser)
    {
        $this->idUser=$idUser;
    }
    
    public function getIdUser()
    {
        return $this->idUser;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
