<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Profile\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Profile\Form\FormResetPassword;

class PasswordController extends AbstractActionController
{
    public function indexAction()
    {
        $view = new ViewModel();
        $locator = $this->getServiceLocator();
        #instancia a query
        $result = $locator->get('HistRecovery');
        
        #parametros
        $token = (string) $this->params()->fromRoute('token', 0);
        $idUser = (int) $this->params()->fromRoute('idUser', 0);
        #Formulario
        $reset = new FormResetPassword('resetPassword', $token, $idUser);
       
        #no existe un parametro se redirecciona
        if($token == 0 && $idUser == 0)
        {
           return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
        }
        #no existe un parametro se redirecciona
        
        #existe token y estado es uno
        if($result->getDataToken($token) && $result->getToken() == $token && $idUser == $result->getIdUser())
        {
            $view->setVariable('reset', $reset);
        }
        else
        {
           $this->getResponse()->setStatusCode(404);
           return; 
        }
        #existe token y estado es uno
        return $view;
    }
}
