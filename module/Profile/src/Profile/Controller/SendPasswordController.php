<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Profile\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Profile\Form\FormResetPassword;
use Profile\Model\Validator\ResetPasswordValidator;

class SendPasswordController extends AbstractActionController
{
    public function indexAction()
    {
        $request = $this->getRequest();
        $controller = $this->params()->fromRoute('controller');
        $action = $this -> params() -> fromRoute('action');
        $locator = $this->getServiceLocator();
        
        $json = array();
        if($request->isPost() && $request->isXmlHttpRequest())
        {
           $form = new FormResetPassword('resetPassword');
           $validator = new ResetPasswordValidator();
           {
              $form->setInputFilter($validator->getInputFilter());
              $form->setData($request->getPost()->toArray());
           }
           
           if($form->isValid())
           {
              $validator->exchangeArray($form->getData()); 
              //echo $validator->ccPassword ." ". $validator->newPassword ." ". $validator->token ." ". $validator->idUser;
              
              $CatUser = $locator->get('CatUser');
              if($CatUser->getDataPassword($validator->idUser))
              {
                  $insert = $locator->get('insert');
                  $update = $locator->get('update');
                  
                  $insert->setTable('histSessionFail');
                  $insert->setInsert(
                    array('idUser' => $validator->idUser), 
                    $controller, 
                    $action
                  );
                 
                  $insert->setTable('histPass');
                  $insert->setInsert(
                    array('passwordPrevious' => $CatUser->getPassword(), 'idUser' => $validator->idUser), 
                    $controller, 
                    $action
                  );
    
                  $update->setTable('catUser');
                  $update->setUpdate(
                    array('password' => md5($validator->ccPassword), 'status' => 1),
                    array('idUser' => $validator->idUser), 
                    $controller, 
                    $action
                 );

                 $update->setTable('histRecovery');
                 $update->setUpdate(
                   array('status' => 0),
                   array('token' => $validator->token, 'idUser' => $validator->idUser), 
                   $controller, 
                   $action
                 );
                 
                 $json['msg'] = ['Sus contraseña fue actualizada.', 'alert'];
              }
           }
           else
           {
             foreach ($form->getMessages() as $key => $value) 
             {
                 foreach ($value as $item => $val) 
                 {
                    $json[$key] = [$val, 'alert'];
                 }
             }
           }
           
           $json = \Zend\Json\Json::encode($json, TRUE);
           if($json) echo \Zend\Json\Json::prettyPrint($json);
           exit();
        }
        return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index')); 
    }
}
