<?php
namespace Profile\Form;
use Zend\Form\Element;
use Zend\Form\Form;

class FormResetPassword extends Form 
{
    public function __construct($name = null, $token = null, $idUser = null)
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        
         $this->add(array(
            'name' => 'newPassword', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array(
                'placeholder' => '**********', 
                'required' => 'required', 
                'maxlength' => 20,
                'pattern' => '(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$'
            ), 
            'options' => array(
                'label' => 'Nueva contraseña',             
            ),
        )); 
        
          $this->add(array(
            'name' => 'ccPassword', 
            'type' => 'Zend\Form\Element\Password', 
            'attributes' => array(
                'placeholder' => '**********', 
                'required' => 'required', 
                'maxlength' => 20,
                'pattern' => '(?=^.{10,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$'
            ), 
            'options' => array(
                'label' => 'Confirmar contraseña',             
            ),
        )); 

        $this->add(array(
            'name' => 'token', 
            'type' => 'Zend\Form\Element\Hidden', 
            'attributes' => array( 
                'value' => isset($token)? $token: FALSE, 
            ), 
            'options' => array(
            ), 
        ));
        
        $this->add(array(
            'name' => 'idUser', 
            'type' => 'Zend\Form\Element\Hidden', 
            'attributes' => array( 
                'value' => isset($idUser)? $idUser: FALSE , 
            ), 
            'options' => array(
            ), 
        ));
        
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Restaurar',
                'class' => 'button span',
            ),
        ));
    } 
} 