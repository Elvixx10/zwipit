<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager -> attach('route', array($this, 'translator'));
    }
    
    public function translator(MvcEvent $e)
    {
       $translator = $e->getApplication()->getServiceManager()->get('translatorApp');
       $translator->setLocale('es');
       $valid = new \Zend\Mvc\I18n\Translator($translator);
       $valid->addTranslationFile(
         'phpArray', 
         './vendor/zendframework/zend-i18n-resources/languages/es/Zend_Validate.php', 
         'default', 
         'es'
       );
       \Zend\Validator\AbstractValidator::setDefaultTranslator($valid);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
