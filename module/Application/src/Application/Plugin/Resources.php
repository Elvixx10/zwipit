<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright  2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */ 
namespace Application\Plugin;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class Resources extends AbstractPlugin 
{
    public function validateImei($imei)
    {
        if (!preg_match('/^[0-9]{15}$/', $imei)) return false;
        $sum = 0;
        for ($i = 0; $i < 14; $i++)
        {
            $num = $imei[$i];
            if (($i % 2) != 0)
            {
                $num = $imei[$i] * 2;
                if ($num > 9)
                {
                    $num = (string) $num;
                    $num = $num[0] + $num[1];
                }
            }
            $sum += $num;
        }
        if ((($sum + $imei[14]) % 10) != 0) return false;
        return true;
    }
}