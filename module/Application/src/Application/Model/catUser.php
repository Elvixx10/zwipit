<?php
namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class catUser implements ServiceLocatorAwareInterface
{
    protected $table = 'catUser';
    protected $password;
    
    public function getDataPassword($idUser)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT password FROM {$this->getTable()} WHERE idUser = {$idUser} AND status IN(1, 0)", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
            foreach ($for as $value) 
            {
                $this->setPassword($value['password']);
            }
            $msg= TRUE;
        }
        return $msg;
    }

    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function setPassword($password)
    {
        $this->password=$password;
    }
    
    public function getPassword()
    {
        return $this->password;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
