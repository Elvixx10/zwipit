<?php
namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Insert implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $table = NULL;
    protected $sql;
    
    public function setInsert(array $data, $controller = NULL, $action = NULL, $id = FALSE)
    {
        $this->sql = new Sql($this->getServiceLocator()->get('Adapter'));
        
        $result = 0;
        $insert = $this->sql->insert($this->getTable())->values($data);
        $statement = $this->sql->prepareStatementForSqlObject($insert);
        try
        {
          $result = $statement->execute() -> getAffectedRows();
          if($id !== FALSE)
          {
               $row = $this->getServiceLocator()->get('Adapter')->query("SELECT IDENT_CURRENT('{$this->getTable()}') AS id", Adapter::QUERY_MODE_EXECUTE);
               foreach ($row->toArray() as $value) {$result = $value['id'];}
          }
        }
        catch(\Exception $e)
        {
          echo $e -> getMessage();
          error_reporting(0);
          $data = array(
            'Controller' => $controller,
            'Action' => $action,                
            'Method' => __METHOD__,
            'Tabla' => $this->getTable(),
            'Query' => $this->sql->getSqlStringForSqlObject($insert)                
          );
          $this->setTable('LogSqlZf2');
          $this->setInsert($data, $controller, $action);
          die();
        }
                
        return $result;
    }
    
    //TODO:getter , setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    //TODO:getter , setter
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
}