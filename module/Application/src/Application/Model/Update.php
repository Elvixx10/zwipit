<?php
namespace Application\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Update implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $table = NULL;
    protected $sql;
    
    public function setUpdate(array $set, array $where, $controller = null, $action = null)
    {
        $this->sql = new Sql($this->getServiceLocator()->get('Adapter'));
        
        $result = 0;
        $update = $this->sql->Update($this->getTable())->set($set)->where($where);
        $statement = $this->sql->prepareStatementForSqlObject($update);
   
        try
        {
            $result = $statement->execute()->getAffectedRows();
        }
        catch(\Exception $e)
        {
            echo $e-> getMessage();
            error_reporting(0);
            $data = array(
                'controller' => $controller,
                'action' => $action,
                'method' => __METHOD__,
                'tabla' => $this->getTable(),
                'query' => $this->sql->getSqlStringForSqlObject($update)                
            );
            
            $insert = $this->getServiceLocator()->get('insert');
            $insert->setTable('LogSqlZf2');
            $insert->setInsert($data, $controller, $action);
            die();
        }       
        return $result;
    }
    
    //TODO:getter , setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    //TODO:getter , setter
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
}

 