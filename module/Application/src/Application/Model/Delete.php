<?php
namespace Application\Model;
use Zend\Db\Sql\Sql;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Delete implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $table = NULL;
    protected $sql;
    
    public function setDelete(array $where)
    {
        $this->sql = new Sql($this->getServiceLocator()->get('Adapter'));
        
        $delete = $this->sql->Delete($this->getTable()) ->where($where);
        $statement = $this->sql->prepareStatementForSqlObject($delete);
        return $statement->execute()->getAffectedRows();
    }
    
    //TODO:getter , setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    //TODO:getter , setter
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
}