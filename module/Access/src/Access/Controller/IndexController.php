<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Access\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
// use PHPExcel;
// use PHPExcel_IOFactory;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        //algoritmo Luhn es un algoritmo
    	$view = new ViewModel();
    	$locator = $this->getServiceLocator();
		$user = $locator->get('&session')->session();
		$view->setVariable('user', $user);
        return $view;
    }
}
