<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

//TODO: tendras que crear el archivo de validación y crear la instancia del formulario para validar del lado del servidor
//por el momento solo lo valide de forma sencilla
use Cart\Form\FormBag; 
 
//TODO: tendras que crear el archivo de validación y crear la instancia del formulario para validar del lado del servidor

class ValidBagController extends AbstractActionController
{
    public function indexAction()
    {
        $locator = $this->getServiceLocator();
        $view = new ViewModel();
        $itemCart = new Container('cart');
        $request = $this->getRequest();
        if($request->isPost() && $request->isXmlHttpRequest())
        {
            $data = $request->getPost();
			//TODO: aqui tienes que realizar una query a la tabla relSachet que tiene relacion con sucursales y cadenas
			//verificar que no este en estado de liberacion dee lo contrario se ejecuta el proceso de forma normal
			if($data->bagIdentifier == 1 || $data->bagIdentifier == 2 || $data->bagIdentifier==3)
			{
			    
                $itemCart->bag=$data->bagIdentifier;
				$view->setVariable('succes', 'block')
					 ->setVariable('code', $data->bagIdentifier);
				
				if($data->bagIdentifier == 1 )
				{
					$view->setVariable('alert', 'ERROR! El número de bolsa proporcionado ya consta como registrado.');
				}
			}
			else
			{
				$view->setVariable('alert', 'ERROR! El número de bolsa proporcionado es incorrecto. Compruebe que ha escrito correctamente todos los caracteres numéricos y letras.');
			}
			$view->setTerminal($request->isXmlHttpRequest());
			
			return $view;
        }       
        return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
    }
}
