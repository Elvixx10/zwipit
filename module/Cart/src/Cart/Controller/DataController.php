<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class DataController extends AbstractActionController
{
    public function indexAction()
    {
        $locator = $this->getServiceLocator();
        $itemCart = new Container('cart');
        $view = new ViewModel();
        $request = $this->getRequest();
        if($request->isPost() && $request->isXmlHttpRequest())
        {
           $data = $request->getPost();
           $itemCart->ife = $data->ife;
           $itemCart->name= $data->name;
           $itemCart->surnames=$data->surnames;
           exit();
        }       
        return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
    }
}
