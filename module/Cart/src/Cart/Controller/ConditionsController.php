<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ConditionsController extends AbstractActionController
{
    public function indexAction()
    {
    	$locator = $this->getServiceLocator();
		$view = new ViewModel();
		$request = $this->getRequest();
		if($request->isGet() && $request->isXmlHttpRequest())
		{
			$data = $request->getQuery();
            $result = $locator->get('Relquestionworks');
            if($result->getConditions($data->statusProduct, $data->chains))
            {
                $view->setVariable('html', $result->getSelect())
                     ->setVariable('conditions', $data->statusProduct);
            }
            
			$view->setTerminal($request->isXmlHttpRequest());
			return $view;
		}		
		return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
    }
}
