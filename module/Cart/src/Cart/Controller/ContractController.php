<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DOMPDFModule\View\Model\PdfModel;
use Zend\Barcode\Barcode;
use Zend\Session\Container;
// use Zend\Config\Config;

class ContractController extends AbstractActionController
{
    public function indexAction()
    {
        //TODO: debes crear una query para sacar el nombre real de la sucursal junto con el domicilio de esta y pasarla a la vista.
        
        $itemCart = new Container('cart');
        $prod = "No Funciona";
        if($itemCart->statusProduct== 1)
        {
            $prod="Funciona";
        }
        
        $pdf = new PdfModel();
        $pdf->setOption("paperSize", "a4"); //Defaults to 8x11
        $pdf->setOption("paperOrientation", "landscape"); //Defaults to portrait
        $pdf->setVariables(array(
          'code' => '3322343432',
          'description' => $itemCart->description,
          'price' => $itemCart->price,
          'imei' => $itemCart->imei,
          'mark' => $itemCart->mark,
          'statusProduct' => $prod,
          'model' => $itemCart->model,
          'ife' => $itemCart->ife,
          'name' => $itemCart->name ." ". $itemCart->surnames
        ));
        return $pdf;
    }
    
    public function barcodeAction()
    {
        // $config = new Config(array(
            // 'barcode'        => 'code39',
            // 'barcodeParams'  => array('text' => 'PV001308488'),
            // 'renderer'       => 'image',
            // 'rendererParams' => array('imageType' => 'png')
        // ));
        //$renderer = Barcode::factory($config)->render();
        $request = $this -> getRequest();
        if($request->isGet())
        {
            $data = $request->getQuery();
            $barcodeOptions = array('text' => $data->code, 'barHeight' => 50);
            $rendererOptions = array('imageType' => 'png');
            Barcode::factory('code39', 'image', $barcodeOptions, $rendererOptions)->render();
        }
        exit();
    }
}
