<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ModelController extends AbstractActionController
{
    public function indexAction()
    {
        $locator = $this->getServiceLocator();
        $view = new ViewModel();
        $request = $this->getRequest();
        if($request->isGet() && $request->isXmlHttpRequest())
        {
            $data = $request->getQuery();
           
            $result = $locator->get('Relequipmentmodel');
            $json[''] = 'Esta marca no contiene modelos';
            
            if($result->getSelectModel($data->idEquipment))
            {
                $json = $result->getSelect();
            }
            
            $json = \Zend\Json\Json::encode($json, TRUE);
            if($json) echo \Zend\Json\Json::prettyPrint($json);
            exit();
        }       
        return $this->redirect()->toRoute('access', array('controller' => 'index', 'action' =>  'index'));
    }
}
