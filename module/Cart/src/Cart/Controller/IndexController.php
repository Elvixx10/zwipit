<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Cart\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cart\Form\FormValuation;
use Cart\Form\FromCustomerData;
use Cart\Form\FormBag;
use Cart\Model\Validator\ValuationValidator;
use Zend\Session\Container;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {    	
    	$locator = $this->getServiceLocator();
        $request = $this->getRequest();
        $view = new ViewModel();
        $itemCart = new Container('cart');
       
        // echo "<pre>".print_r($locator->get('&Session')->session(), TRUE)."</pre>";
        //Armando Reyna, armando.i.reyna@gmail.com
        
        if($request->isPost() && $request->isXmlHttpRequest())
        {
            $data = $request->getPost();
            // echo "<pre>".print_r($data, true)."</pre>";
            $viewSearch = new ViewModel();
            $viewSearch->setTemplate('application/imei-search/imei');
            $result = $locator->get('Relequipmentmodel');
            if($result->getDataImei($data->imei))
            {
                $viewSearch->setVariable('total', number_format($result->getTotal(), 2, '.', ''));
                $viewSearch->setVariable('picture', $result->getPicture());
                $viewSearch->setVariable('imei', $result->getImei());
                $viewSearch->setVariable('description', $result->getDescription());
                $viewSearch->setVariable('mark', $result->getMark());
                $viewSearch->setVariable('model', $result->getModel());
                
                //TODO:$data->statusProduct con este parametro debes ir a buscar al catalago catWorks el texto para saber el status del producto
                $viewSearch->setVariable('statusProduct',  $data->statusProduct);
            }
            else 
            {
                if($this->Resources()->validateImei($data->imei))
                {
                   $viewSearch->setVariable('success', 'IMEI correcto');
                   $viewSearch->setVariable('msg', "No se encontraron resultados para el IMEI {$data->imei}. Intente buscando por marca/modelo.");
                }
                else
                {
                   $viewSearch->setVariable('alert', 'IMEI incorrecto');
                }
            }
            $viewSearch->setTerminal($request->isXmlHttpRequest());
            $config = $locator->get('ViewRenderer');
            echo $config->render($viewSearch);
            exit();
        }
        elseif ($request->isPost()) 
        {
            $data = $request->getPost();
            $prod = "No Funciona";
            if($data->statusProductSession== 1)
            {
                $prod="Funciona";
            }
        
            $itemCart->description = $data->descriptionSession;
            $itemCart->price = $data->totalSession;
            $itemCart->imei = $data->imeiSession;
            $itemCart->mark = $data->markSession;
            $itemCart->statusProduct = $data->statusProductSession;
            $itemCart->model = $data->modelSession;
            $view->setVariable('addCart', "Añadido el producto {$data->descriptionSession} con estado '{$prod}' y precio {$data->totalSession}"); 
        }


        if($request->isGet())
        {
            $data = $request->getQuery();
            if($data->deleteCart == 0)
            {
                $itemCart->getManager()->getStorage()->clear();
            }
        }

        $form = new FormValuation('valuation', $locator);
        $form2 =  new FromCustomerData('customerData');
		$form3 = new FormBag('bagIdentifier');
        
        $view->setVariable('valuation', $form)
             ->setVariable('desc', $itemCart->description)
             ->setVariable('price', $itemCart->price)
             ->setVariable('imei', $itemCart->imei)
             ->setVariable('mark', $itemCart->mark)
             ->setVariable('model', $itemCart->model)
             ->setVariable('bag', $itemCart->bag)
             ->setVariable('statusProduct', $itemCart->statusProduct)
             ->setVariable('customerData', $form2)
			 ->setVariable('bagIdentifier', $form3);
        return $view;
    }
}
