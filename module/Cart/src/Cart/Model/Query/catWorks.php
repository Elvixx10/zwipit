<?php
namespace Cart\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class catWorks implements ServiceLocatorAwareInterface
{
    protected $table = 'catWorks';
    protected $idWorks;
    protected $desWorks;
    protected $date;
    protected $status = 1;
	protected $select = array();
    protected $serviceLocator;
    
    public function getSelectStatusProduct()
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT idWorks, desWorks FROM {$this->getTable()} WHERE status = {$this->getStatus()} ORDER BY desWorks ASC", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
        	$array['']  = 'Seleccione';
            foreach ($for as $value) {$array[$value['idWorks']]= $value['desWorks'];}
			$this->setSelect($array);
            $msg= TRUE;
        }
        return $msg;
    }
	
	public function setSelect(array $array)
	{
		$this->select=$array;
	}
	
	public function getSelect()
	{
		return $this->select;
	}

    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
	
	public function setIdWorks($idProduct)
    {
        $this->idProduct=$idProduct;
    }
    
    public function getIdWorks()
    {
        return $this->idProduct;
    }
	
	public function setDesWorks($desProduct)
	{
		$this->desProduct=$desProduct;
	}
	
	public function getDesWorks()
	{
		return $this->desProduct;
	}
	
	public function setDate($date)
    {
        $this->date=$date;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
	public function setStatus($status)
	{
		$this->status=$status;
	}
	
	public function getStatus()
	{
		return $this->status;
	}
	
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}