<?php
namespace Cart\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class relchainsequipment implements ServiceLocatorAwareInterface
{
    protected $table = 'relchainsequipment';
    // protected $idQuestionWorks;
    // protected $desQuestion;
    // protected $percent;
    // protected $idWorks;
    // protected $idChains;
    // protected $date;
    // protected $status = 1;
    protected $serviceLocator;
    protected $select = array();

    public function getSelectMark($idChains, $idBranchOffice, $idProduct)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT a.setting as SChains , b.idEquipment, b.mark, c.setting AS SBranchOffice 
        FROM {$this->getTable()} AS a
        INNER JOIN catequipment AS b ON b.idEquipment=a.idEquipment AND b.status=1
        INNER JOIN relbranchofficechains AS c ON a.idChainsEquipment=c.idChainsEquipment AND c.status =1
        WHERE a.idChains={$idChains} AND c.idBranchOffice={$idBranchOffice} AND b.idProduct={$idProduct}  AND a.status=1", Adapter::QUERY_MODE_EXECUTE);
        
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
            $array[''] = 'Seleccione';
            foreach ($for as $value) 
            {
                 $array[$value['idEquipment']] = $value['mark'];
            }
            $this->setSelect($array);
            $msg= TRUE;
        }
        return $msg;
    }
    
    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function setSelect(array $select)
    {
        $this->select=$select;
    }
    
    public function getSelect()
    {
        return $this->select;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
