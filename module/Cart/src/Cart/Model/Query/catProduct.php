<?php
namespace Cart\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class catProduct implements ServiceLocatorAwareInterface
{
    protected $table = 'catProduct';
    protected $idProduct;
    protected $desProduct;
    protected $date;
    protected $status = 1;
	protected $select = array();
    
    public function getSelectProduct()
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT idProduct, desProduct  FROM {$this->getTable()} WHERE status = {$this->getStatus()} ORDER BY desProduct ASC", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
        	$array['']  = 'Seleccione';
            foreach ($for as $value) {$array[$value['idProduct']]= $value['desProduct'];}
			$this->setSelect($array);
            $msg= TRUE;
        }
        return $msg;
    }
	
	public function setSelect(array $array)
	{
		$this->select=$array;
	}
	
	public function getSelect()
	{
		return $this->select;
	}

    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
	
	public function setIdProduct($idProduct)
    {
        $this->idProduct=$idProduct;
    }
    
    public function getIdProduct()
    {
        return $this->idProduct;
    }
	
	public function setDesProduct($desProduct)
	{
		$this->desProduct=$desProduct;
	}
	
	public function getDesProduct()
	{
		return $this->desProduct;
	}
	
	public function setDate($date)
    {
        $this->date=$date;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
	public function setStatus($status)
	{
		$this->status=$status;
	}
	
	public function getStatus()
	{
		return $this->status;
	}
	
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
