<?php
namespace Cart\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class relequipmentmodel implements ServiceLocatorAwareInterface
{
    protected $table = 'relequipmentmodel';
    protected $total;
    protected $mark;
    protected $model;
    protected $picture;
    protected $description;
    protected $imei;    
    protected $status = 1;
    protected $serviceLocator;
    protected $select = array();

    public function getSelectModel($idEquipment)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT idEquipmentModel, model FROM {$this->getTable()} WHERE idEquipment = {$idEquipment} AND status={$this->getStatus()}", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
            $array=[];
            foreach ($for as $value) 
            {
                 $array[$value['idEquipmentModel']] = $value['model'];
            }
            $this->setSelect($array);
            $msg= TRUE;
        }
        return $msg;
    }
    
    public function getDataImei($imei)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT (a.setting*((b.setting/100)+1)) as 'total', c.mark, d.model, d.picture, d.description, d.imei 
        FROM relchainsequipment AS a
        INNER JOIN relbranchofficechains AS b ON b.idChainsEquipment=a.idChainsEquipment AND b.status={$this->getStatus()}
        INNER JOIN catequipment AS c ON c.idEquipment = a.idEquipment AND c.status={$this->getStatus()}
        INNER JOIN relequipmentmodel AS d ON d.idEquipment = a.idEquipment AND d.status={$this->getStatus()}
        WHERE d.imei = '{$imei}' AND a.status={$this->getStatus()}", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {     
            foreach ($for as $value) 
            {
                $this->setTotal($value['total']);
                $this->setMark($value['mark']);
                $this->setModel($value['model']);
                $this->setPicture($value['picture']);
                $this->setDescription($value['description']);
                $this->setImei($value['imei']);
            }            
            $msg= TRUE;
        }
        return $msg;
    }
    
    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }    
    
    public function setTotal($total)
    {
        $this->total=$total;
    }
    
    public function getTotal()
    {
        return $this->total;
    }
    
    public function setMark($mark)
    {
        $this->mark=$mark;
    }
    
    public function getMark()
    {
        return $this->mark;
    }
    
    public function setModel($model)
    {
        $this->model=$model;
    }
    
    public function getModel()
    {
        return $this->model;
    }
    
    public function setPicture($picture)
    {
        $this->picture=$picture;
    }
    
    public function getPicture()
    {
        return $this->picture;
    }
    
    public function setDescription($description)
    {
        $this->description=$description;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setImei($imei)
    {
        $this->imei=$imei;
    }
    
    public function getImei()
    {
        return $this->imei;
    }
    
    public function setStatus($status)
    {
        $this->status=$status;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function setSelect(array $select)
    {
        $this->select=$select;
    }
    
    public function getSelect()
    {
        return $this->select;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
