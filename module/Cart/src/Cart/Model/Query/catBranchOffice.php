<?php
namespace Cart\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class catBranchOffice implements ServiceLocatorAwareInterface
{
    protected $table = 'catBranchOffice';
    protected $idBranchOffice;
    protected $name;	
	protected $address;
	protected $inCharge;
	protected $phone;
	protected $comision;
    protected $date;
    protected $status = 1;
    protected $idChains;
	protected $idCollectionCenter;
	protected $idPayments;
    protected $serviceLocator;
	
    public function getChains($idBranchOffice)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT idChains FROM {$this->getTable()} WHERE idBranchOffice={$idBranchOffice} AND status = {$this->getStatus()}", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {    	
            foreach ($for as $value) 
            {
            	$this->setIdChains($value['idChains']);
			}
            $msg= TRUE;
        }
        return $msg;
    }
	
    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
	
	public function setIdBranchOffice($idBranchOffice)
	{
		$this->idBranchOffice=$idBranchOffice;
	}
	
	public function getIdBranchOffice()
	{
		return $this->idBranchOffice;
	}
	
	public function setName($name)
	{
		$this->name=$name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setAddress($address)
	{
		$this->address=$address;
	}
	
	public function getAddress()
	{
		return $this->address;
	}
	
	public function setInCharge($inCharge)
	{
		$this->inCharge=$inCharge;
	}
	
	public function getInCharge()
	{
		return $this->inCharge;
	}
	
	public function setPhone($phone)
	{
		$this->phone=$phone;
	}
	
	public function getPhone()
	{
		return $this->phone;
	}
	
	public function setComision($comision)
	{
		$this->comision=$comision;
	}
	
	public function getComision()
	{
		return $this->comision;
	}
	
	public function setDate($date)
    {
        $this->date=$date;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
	public function setStatus($status)
	{
		$this->status=$status;
	}
	
	public function getStatus()
	{
		return $this->status;
	}
	
	public function setIdChains($idChains)
	{
		$this->idChains=$idChains;
	}
	
	public function getIdChains()
	{
		return $this->idChains;
	}
	
	public function setIdCollectionCenter($idCollectionCenter)
	{
		$this->idCollectionCenter=$idCollectionCenter;
	}
	
	public function getIdCollectionCenter()
	{
		return $this->idCollectionCenter;
	}
	
	public function setIdPayments($idPayments)
	{
		$this->idPayments=$idPayments;
	}
	
	public function getIdPayments()
	{
		return $this->idPayments;
	}
	
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
