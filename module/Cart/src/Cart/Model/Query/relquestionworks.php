<?php
namespace Cart\Model\Query;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class relquestionworks implements ServiceLocatorAwareInterface
{
    protected $table = 'relquestionworks';
    protected $idQuestionWorks;
    protected $desQuestion;
    protected $percent;
    protected $idWorks;
    protected $idChains;
    protected $date;
    protected $status = 1;
    protected $serviceLocator;
    protected $select = array();

    public function getConditions($idWorks, $idChains)
    {
        $result = $this->getServiceLocator()->get('Adapter')->query("SELECT idQuestionWorks, desQuestion, percent FROM {$this->getTable()} WHERE status = {$this->getStatus()} AND idWorks = {$idWorks} AND idChains = {$idChains}", Adapter::QUERY_MODE_EXECUTE);
        $for = $result->toArray();
        $msg = FALSE;
        if(!empty($for))
        {
            $array = [];
            foreach ($for as $value) 
            {
                $array[]= array(
                    'id' => $value['idQuestionWorks'],
                    'des' => $value['desQuestion'], 
                    'percent' => $value['percent']
                );
            }
            $this->setSelect($array);
            $msg= TRUE;
        }
        return $msg;
    }
    
    public function setSelect(array $select)
    {
        $this->select=$select;
    }
    
    public function getSelect()
    {
        return $this->select;
    }

    //getter y setter
    public function setTable($table)
    {
        $this->table=$table;
    }
    
    public function getTable()
    {
        return $this->table;
    }
    
    public function setIdQuestionWorks($idQuestionWorks)
    {
        $this->idQuestionWorks=$idQuestionWorks;
    }
    
    public function getIdQuestionWorks()
    {
        return $this->idQuestionWorks;
    }
    
    public function setDesQuestion($desQuestion)
    {
        $this->desQuestion=$desQuestion;
    }
    
    public function getDesQuestion()
    {
        return $this->desQuestion;
    }
    
    public function setPercent($percent)
    {
        $this->percent=$percent;
    }
    
    public function getPercent()
    {
        return $this->percent;
    }
    
    public function setIdWorks($idWorks)
    {
        $this->idWorks=$idWorks;
    }
    
    public function getIdWorks()
    {
        return $this->idWorks;
    }
    
    public function setIdChains($idChains)
    {
        $this->idChains=$idChains;
    }
    
    public function getIdChains()
    {
        return $this->idChains;
    }
    
    public function setDate($date)
    {
        $this->date=$date;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function setStatus($status)
    {
        $this->status=$status;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this -> serviceLocator;
    }
    //getter y setter
}
