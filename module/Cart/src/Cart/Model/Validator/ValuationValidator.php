<?php
namespace Cart\Model\Validator;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Regex;

class ValuationValidator implements InputFilterAwareInterface 
{
	public $product;
	public $statusProduct;
	public $imei;
	public $mark;
	public $model;
	 
    protected $inputFilter; 
    
	public function exchangeArray($data)
    {
        $this->product  = (isset($data['product'])) ? $data['product'] : null;
        $this->statusProduct   = (isset($data['statusProduct'])) ? $data['statusProduct'] : null;
        $this->imei = (isset($data['imei'])) ? $data['imei'] : null;
        $this->mark = (isset($data['mark'])) ? $data['mark'] : null;
		$this->model = (isset($data['model'])) ? $data['model'] : null;
    }
	
	public function getArrayCopy()
    {
        return get_object_vars($this);
    }
	
    public function setInputFilter(InputFilterInterface $inputFilter) 
    { 
        throw new \Exception("Not used"); 
    }
	
    public function getInputFilter() 
    { 
        if (!$this->inputFilter) 
        { 
            $inputFilter = new InputFilter(); 
            $factory = new InputFactory(); 
            
        
        $inputFilter->add($factory->createInput([ 
            'name' => 'product', 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array(
            	array ( 
                    'name' => 'InArray', 
                    'options' => array( 
                            'haystack' => array(0) 
                        'messages' => array(, 
                            'notInArray' => 'undefined' 
                        ), 
                    ), 
                ),  
            ), 
        ])); 
 
        $inputFilter->add($factory->createInput([
            'name' => 'statusProduct', 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array ( 
                    'name' => 'InArray', 
                    'options' => array(
                        'haystack' => array(0),
                        'messages' => array(, 
                            'notInArray' => 'undefined' 
                        ), 
                    ), 
                ), 

            ), 
        ]));
        
        $inputFilter->add($factory->createInput([ 
            'name' => 'searchOptions', 
            'filters' => array( 
              array('name' => 'StripTags'), 
              array('name' => 'StringTrim'), 
            ), 
            'validators' => array(
            ), 
        ]));
        
        $inputFilter->add($factory->createInput([
            'name' => 'serial', 
            'required' => false, 
            'filters' => array(
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array(
                array ( 
                    'name' => 'StringLength', 
                    'options' => array(
                        'encoding' => 'UTF-8', 
                        'min' => '30', 
                    ), 
                ), 
            ), 
        ]));
 
        $inputFilter->add($factory->createInput([
            'name' => 'imei', 
            'required' => true, 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array ( 
                    'name' => 'StringLength', 
                    'options' => array( 
                        'encoding' => 'UTF-8', 
                        'min' => '15', 
                    ), 
                ), 
            ), 
        ]));
 
        $inputFilter->add($factory->createInput([ 
            'name' => 'mark', 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array ( 
                    'name' => 'InArray', 
                    'options' => array( 
                        'haystack' => array(0),
                        'messages' => array(, 
                            'notInArray' => 'undefined' 
                        ), 
                    ), 
                ), 

            ), 
        ])); 
 
        $inputFilter->add($factory->createInput([ 
            'name' => 'model', 
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array ( 
                    'name' => 'InArray', 
                    'options' => array( 
                        'haystack' => array(0),
                        'messages' => array(, 
                            'notInArray' => 'undefined' 
                        ), 
                    ), 
                ), 

            ), 
        ])); 
 
            $this->inputFilter = $inputFilter; 
        } 
        
        return $this->inputFilter; 
    } 
} 