<?php
namespace Cart\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class FormBag extends Form 
{    
    public function __construct($name = null, $locator = null)
    {       
        if($locator !== null){}
        
        parent::__construct($name);
		
        $this->setAttribute('method', 'post');
		
        $this->add(array(
            'name' => 'bagIdentifier', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Ejemplo: 3434334', 
                'required' => 'required', 
                'maxlength' => 10
            ), 
            'options' => array(
                'label' => 'Código numérico', 
            ), 
        ));
        
       
        $this->add(array(
            'name' => 'validBag',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Validar Bolsa',
                'class' => 'button span',
            ),
        ));
    } 
}