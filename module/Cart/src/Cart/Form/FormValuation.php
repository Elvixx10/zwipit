<?php
namespace Cart\Form;

use Zend\Form\Element; 
use Zend\Form\Form; 

class FormValuation extends Form 
{ 
    public function __construct($name = null, $locator = null) 
    {    	
		if($locator !== null)
		{
			$selectPro = $locator->get('CatProduct');
			$valuePro ='';
			if($selectPro->getSelectProduct()){$valuePro = $selectPro->getSelect();}
			
			$selectStatus = $locator->get('CatWorks');
			$valueStatus= '';
			if($selectStatus->getSelectStatusProduct()){$valueStatus = $selectStatus->getSelect();}
			
			$user = $locator->get('&Session')->session();
			$query = $locator->get('RelDataUser');
            
            $valueBranchOffice = '';
            $valueMark = '';
            
			if($query->getBranchOffice($user['idUser']))
			{
			    $valueBranchOffice = $query->getIdBranchOffice();
				$valueChains= '';
				$chains = $locator->get('catBranchOffice');
				if($chains->getChains($valueBranchOffice))
				{
				    $valueChains= $chains->getIdChains();
                }
                
                $mark = $locator->get('Relchainsequipment');
                if($mark->getSelectMark($valueChains, $valueBranchOffice, 2)) //2 solo tabletas.
                {
                    $valueMark = $mark->getSelect();
                }
			}		
		}
		
        parent::__construct($name);
        
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'product', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array(
                'required' => 'required',
                'value' => '1', 
            ), 
            'options' => array( 
            	'label' => 'Seleccionar el tipo de producto', 
                'value_options' => $valuePro = (!empty($valuePro)) ? $valuePro : array()
            ), 
        )); 
 
        $this->add(array(
            'name' => 'statusProduct', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'required' => 'required', 
            ), 
            'options' => array( 
                'value_options' => $valueStatus = (!empty($valueStatus)) ? $valueStatus : array()
            ), 
        )); 
		
		$this->add(array(
            'name' => 'chains', 
            'type' => 'Zend\Form\Element\Hidden', 
            'attributes' => array( 
                'value' => $valueChains, 
            ), 
            'options' => array(
                //'label' => '',
            ), 
        ));
        
        $this->add(array(
            'name' => 'idBranchOffice', 
            'type' => 'Zend\Form\Element\Hidden', 
            'attributes' => array( 
                'value' => $valueBranchOffice, 
            ), 
            'options' => array(
                //'label' => '',
            ), 
        ));
        
        $this->add(array(
            'name' => 'searchOptions', 
            'type' => 'Zend\Form\Element\Radio', 
            'attributes' => array( 
                'required' => 'required', 
                'value' => 'imei', 
            ), 
            'options' => array( 
                // 'label' => 'Busqueda', 
                'value_options' => array(
                    'imei' => 'Buscar por IMEI', 
                    'make_model' => 'Buscar por Marca/Modelo', 
                ),
            ), 
        )); 
        
        $this->add(array(
            'name' => 'serial', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => '7812AB3', 
                // 'required' => 'required', 
                'maxlength' => 30
            ), 
            'options' => array(
                'label' => 'Número de serie', 
            ), 
        ));
 
        $this->add(array(
            'name' => 'imei', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => '0120183892927383', 
                'required' => 'required', 
                'maxlength' => 15
            ), 
            'options' => array( 
                'label' => 'Introducir IMEI', 
            ), 
        ));
        

        $this->add(array(
            'name' => 'mark', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                // 'required' => 'required', 
            ), 
            'options' => array( 
                'label' => 'Marca', 
                'value_options' => $valueMark
            ), 
        ));
 
        $this->add(array(
            'name' => 'model', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                // 'required' => 'required', 
            ), 
            'options' => array(
                'label' => 'Modelo', 
                'value_options' => array(
                    '' => 'Seleccione', 
                ),
            ), 
        ));
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Buscar',
                'class' => 'button span',
            ),
        ));
    } 
}