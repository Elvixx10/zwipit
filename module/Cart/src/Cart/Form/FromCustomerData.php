<?php
namespace Cart\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class FromCustomerData extends Form 
{
    
    public $day = array('' => 'Seleccione');
    public $month= array('' => 'Seleccione');
    public $year = array('' => 'Seleccione');
     
    public function __construct($name = null, $locator = null)
    {       
        if($locator !== null){}
        
        for ($i=1; $i <=  31; $i++) 
        {
            if($i <= 9) $i="0".$i ;
            $this->day[$i] = $i;
        }   
        
        for ($a=1; $a <= 12; $a++) 
        { 
           if($a <= 9) $a="0".$a ;
           $this->month[$a] = $a;
        } 
        
        for ($z=date('Y'); $z >= 1915; $z--) 
        { 
            $this->year[$z] = $z;
        }
        
        parent::__construct($name);
        
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'ife', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Mi Identificación', 
                'required' => 'required', 
                'maxlength' => 13
            ), 
            'options' => array( 
                'label' => 'Identificación oficial', 
            ), 
        ));
 
        $this->add(array(
            'name' => 'customer', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'required' => 'required', 
            ), 
            'options' => array(
                'value_options' => array(
                    '' => 'Seleccione',
                    'mr' => 'Sr.',
                    'mrs' => 'Sra.'
                ),
                'label' => 'Tratamiento', 
            ),
        ));
        
        $this->add(array(
            'name' => 'name', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Juan', 
                'required' => 'required', 
                'maxlength' => 25
            ), 
            'options' => array( 
                'label' => 'Nombre', 
            ), 
        ));
        
        $this->add(array(
            'name' => 'surnames', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Perez Cruz', 
                'required' => 'required', 
                'maxlength' => 35
            ), 
            'options' => array( 
                'label' => 'Apellidos', 
            ), 
        ));
        
        #---
        $this->add(array(
            'name' => 'day', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'required' => 'required', 
            ), 
            'options' => array(
                'value_options' => $this->day,
                'label' => 'Dia', 
            ), 
        ));
        
        $this->add(array(
            'name' => 'month', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'required' => 'required', 
            ), 
            'options' => array(
                'value_options' => $this->month,
                'label' => 'Mes', 
            ), 
        ));
        
        $this->add(array(
            'name' => 'year', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'required' => 'required', 
            ), 
            'options' => array(
                'value_options' => $this->year,
                'label' => 'Año' 
            ), 
        ));
        #---
       
        
       #--forma de pago
       
       //TODO: este campo se tien que llenar con base al catalgo catPayments que esta relacionada con catBranchOffice
        $this->add(array(
            'name' => 'payments', 
            'type' => 'Zend\Form\Element\Radio', 
            'attributes' => array(
                'required' => 'required', 
                'value' => '1' 
            ), 
            'options' => array( 
                'label' => 'Forma de pago', 
                'value_options' => array(
                    '1' => 'Vale descuento',                     
                ),
            ), 
        )); 
		
		//TODO: este campo se tien que llenar con base al catalgo catPayments que esta relacionada con catBranchOffice
       
       $this->add(array(
            'name' => 'Coupon', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array( 
                'placeholder' => 'Ejemplo: 324334244343',
                'required' => 'required', 
                'maxlength' => 20
            ), 
            'options' => array( 
                'label' => 'Vale descuento', 
            ), 
        ));
        
       #--forma de pago
       
        $this->add(array(
            'name' => 'next',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Siguiente',
                'class' => 'button span',
            ),
        ));
    } 
}