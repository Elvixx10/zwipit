<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'cart' => array(
                'type'    => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/cart',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Cart\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
        'invokables' => array(
        	'CatProduct' => 'Cart\Model\Query\catProduct',
        	'CatWorks' => 'Cart\Model\Query\catWorks',
        	'CatBranchOffice' => 'Cart\Model\Query\catBranchOffice',
        	'Relquestionworks' => 'Cart\Model\Query\relquestionworks',
        	'Relchainsequipment' => 'Cart\Model\Query\relchainsequipment',
        	'Relequipmentmodel' => 'Cart\Model\Query\relequipmentmodel'
		)
    ),
    'translator' => array(
        'locale' => 'es_ES',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Cart\Controller\Index' => 'Cart\Controller\IndexController',
            'Cart\Controller\Conditions' => 'Cart\Controller\ConditionsController',
            'Cart\Controller\Model' => 'Cart\Controller\ModelController',
            'Cart\Controller\CatMark' => 'Cart\Controller\CatMarkController',
            'Cart\Controller\ValidBag' => 'Cart\Controller\ValidBagController',
            'Cart\Controller\Contract' => 'Cart\Controller\ContractController',
            'Cart\Controller\Data' => 'Cart\Controller\DataController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'template_map' => array(
            'layout/cart'           => __DIR__ . '/../view/layout/cart.phtml'
        ),
        'template_path_stack' => array(
             __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
