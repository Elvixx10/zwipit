<?php
// ./e_errorhandler.php in root of ZF2 app
//adapt from https://samsonasik.wordpress.com/2014/01/21/zend-framework-2-handle-e-php-error/

define('E_FATAL', E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR);

define('DISPLAY_ERRORS', TRUE);
define('ERROR_REPORTING', E_ALL | E_STRICT);

register_shutdown_function('shut');
set_error_handler('handler');

function shut()
{
    $error = error_get_last();
    if ($error && ($error['type'] & E_FATAL)) {
        handler($error['type'], $error['message'], $error['file'], $error['line']);
    }
}

function handler($errno, $errstr, $errfile, $errline)
{
    switch ($errno)
    {
        case E_ERROR :
            $typestr = 'E_ERROR';
        break;
        
        case E_WARNING :
            $typestr = 'E_WARNING';
        break;
        
        case E_PARSE :
            $typestr = 'E_PARSE';
        break;
        
        case E_NOTICE :
            $typestr = 'E_NOTICE';
        break;
        
        case E_CORE_ERROR :
            $typestr = 'E_CORE_ERROR';
        break;
        
        case E_CORE_WARNING :
            $typestr = 'E_CORE_WARNING';
        break;
        
        case E_COMPILE_ERROR :
            $typestr = 'E_COMPILE_ERROR';
        break;
        
        case E_CORE_WARNING :
            $typestr = 'E_COMPILE_WARNING';
        break;
        
        case E_USER_ERROR :
            $typestr = 'E_USER_ERROR';
        break;
        
        case E_USER_WARNING :
            $typestr = 'E_USER_WARNING';
        break;
        
        case E_USER_NOTICE :
            $typestr = 'E_USER_NOTICE';
        break;
        
        case E_STRICT :
            $typestr = 'E_STRICT';
        break;
        
        case E_RECOVERABLE_ERROR :
            $typestr = 'E_RECOVERABLE_ERROR';
        break;
        
        case E_DEPRECATED :
            $typestr = 'E_DEPRECATED';
        break;
        
        case E_USER_DEPRECATED :
            $typestr = 'E_USER_DEPRECATED';
        break;
    }
 
    $message = " Error PHP in file : " . $errfile . " at line : " . $errline . "
    with type error : " . $typestr . " : " . $errstr . " in " . $_SERVER['REQUEST_URI'];

    if (!($errno & ERROR_REPORTING)) {
        return;
    }

    if (DISPLAY_ERRORS) {
        //logging...
        $logger = new Zend\Log\Logger;

        //stream writer
        $writerStream = new Zend\Log\Writer\Stream(__DIR__ . '/data/logs/' . date('Y-m-d') . '-log.txt');
        $logger->addWriter($writerStream);
        //log it!
        $logger->crit($message);
        
        //include __DIR__ . '/module/Application/view/error/e_handler.phtml';
        //die();
    }
}
